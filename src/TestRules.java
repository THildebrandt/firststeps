import jasima.core.experiment.FullFactorialExperiment;
import jasima.core.util.ConsolePrinter;
import jasima.core.util.ExcelSaver;
import jasima.shopSim.core.PR;
import jasima.shopSim.models.staticShop.StaticShopExperiment;
import jasima.shopSim.prioRules.basic.EDD;
import jasima.shopSim.prioRules.basic.FCFS;
import jasima.shopSim.prioRules.basic.SPT;
import jasima.shopSim.prioRules.basic.SRPT;
import jasima.shopSim.prioRules.basic.TieBreakerFASFS;
import jasima.shopSim.prioRules.upDownStream.IFTMinusUITPlusNPT;
import jasima.shopSim.prioRules.upDownStream.PTPlusWINQPlusNPT;
import jasima.shopSim.util.BasicJobStatCollector;

public class TestRules {

	public static void main(String[] args) {
		// Basis-Experiment erzeugen
		StaticShopExperiment e = new StaticShopExperiment();
		// Instanz-Name richtig einstellen
		e.setInstFileName("js20x05.txt");

		// Listener für zusätzliche Auswertungen, ggfs. Zeile 22 wieder
		// aktvieren, um den TraceFileProducer zu aktivieren
		e.addShopListener(new BasicJobStatCollector());
		// e.addShopListener(new TraceFileProducer());

		// FullFactorialExperiment, um verscheidene Einstellungen zu testen
		// (hier: Prioritätsregeln)
		FullFactorialExperiment ffe = new FullFactorialExperiment();
		// Experiment "e" soll mit verschiednene Einstellungen getestet werden
		ffe.setBaseExperiment(e);

		// zu testende Regeln erzeugen
		Object[] rules = createRules();
		// Attribut "sequencingRule" des Experimente "e" soll getestet werden
		ffe.addFactors("sequencingRule", rules);

		// Ergebnisse als Excel-Datei speichern
		ffe.addNotifierListener(new ExcelSaver());
		// zusätzliche Ausgaben während Ausführung des Experiments
		ffe.addNotifierListener(new ConsolePrinter());
		// Experiment ausführen
		ffe.runExperiment();
		
		// Ergebnisse zusätzlich nochmal auf der Konsole ausgeben
		ffe.printResults();
	}

	private static Object[] createRules() {
		SPT spt = new SPT();
		spt.setTieBreaker(new TieBreakerFASFS());

		FCFS fcfs = new FCFS();
		fcfs.setTieBreaker(new TieBreakerFASFS());

		EDD edd = new EDD();
		edd.setTieBreaker(new TieBreakerFASFS());

		SRPT srpt = new SRPT();
		srpt.setTieBreaker(new TieBreakerFASFS());

		PTPlusWINQPlusNPT ptPlusWINQPlusNPT = new PTPlusWINQPlusNPT();
		ptPlusWINQPlusNPT.setTieBreaker(new TieBreakerFASFS());

		IFTMinusUITPlusNPT iftMinusUITPlusNPT = new IFTMinusUITPlusNPT();
		iftMinusUITPlusNPT.setTieBreaker(new TieBreakerFASFS());

		return new PR[] { spt, fcfs, edd, srpt, ptPlusWINQPlusNPT, iftMinusUITPlusNPT };
	}

}
